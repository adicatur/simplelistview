package com.example.rahardyan.myapplication;

/**
 * Created by angga on 6/24/16.
 */
public class Food {
    private String name;
    private String taste;

    public Food(String name, String taste) {
        this.name = name;
        this.taste = taste;
    }

    public String getName() {
        return name;
    }

    public String getTaste() {
        return taste;
    }


    @Override
    public String toString() {
        return "Food{" +
                "name='" + name + '\'' +
                ", taste='" + taste + '\'' +
                '}';
    }
}
