package com.example.rahardyan.myapplication;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;


/**
 * Created by rahardyan on 23/06/16.
 */
public class ListAdapterFood extends BaseAdapter{
    private List<Food> listFoods;
    private LayoutInflater inflater;

    public ListAdapterFood(List<Food> listFoods, Context context) {
        this.listFoods = listFoods;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return listFoods.size();
    }

    @Override
    public Food getItem(int position) {
        return listFoods.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Holder holder;

        if(convertView == null){
            convertView = inflater.inflate(R.layout.item_food,parent,false);
            holder = new Holder();
            holder.textViewName = (TextView) convertView.findViewById(R.id.text_name);
            holder.textViewTaste = (TextView) convertView.findViewById(R.id.text_taste);
            convertView.setTag(holder);
        }else{
            holder = (Holder) convertView.getTag();
        }

        Food food = listFoods.get(position);

        holder.textViewName.setText(food.getName());
        holder.textViewTaste.setText(food.getTaste());

        return convertView;
    }

    static class Holder{
        TextView textViewName;
        TextView textViewTaste;
    }
}
