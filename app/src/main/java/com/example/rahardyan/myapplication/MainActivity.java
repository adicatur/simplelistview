package com.example.rahardyan.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    String[] data = {"a", "b", "c", "d"};
    private ListView listView;
    private ArrayAdapter adapter;
    private ListAdapterFood listAdapterFood;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = (ListView) findViewById(R.id.list_view);

        listAdapterFood = new ListAdapterFood(getFood(),getApplicationContext());

        listView.setAdapter(listAdapterFood);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(MainActivity.this, "ini item " + getFood().get(position).getName() , Toast.LENGTH_SHORT).show();
            }
        });
    }

    private Food get(String name,String taste) {
        return new Food(name,taste);
    }

    private List<Food> getFood() {
        List<Food> list = new ArrayList<Food>();
        list.add(get("Apple","Yahood"));
        list.add(get("Bamboo","Jooss"));
        list.add(get("Mango","Makyuuss"));
        list.add(get("Orange","Yooosh"));

        return list;
    }
}
